import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import Stack from '@mui/material/Stack';
import Avatar from '@mui/material/Avatar';
import AlertMes from '../../menu/components/Alert';

export default function CategoryFormEdit(props) {
    const [categoryData, setCategoryData] = React.useState({ name: props.categoryData?.name, title: props.categoryData?.title, icon: '' })
    const [alert, setAlert] = React.useState({ message: '', severity: '', open: false });
    const [icon, setIcon] = React.useState(`${process.env.REACT_APP_URL}/${props.categoryData?.icon}`);
    const handleClickAdd = async () => {
        const categoryForm = new FormData()
        categoryForm.append('name', categoryData.name)
        categoryForm.append('title', categoryData.title)
        categoryForm.append('icon', categoryData.icon)
        props.setEditCategory({ ...props.editCategory, open: false, categoryInfo: categoryForm, yesOrNo: true });
    };

    const handleClose = () => {
        props.setEditCategory({ ...props.editCategory, open: false });
    };


    function handleChange(e) {
        setCategoryData({ ...categoryData, icon: e.target.files[0] })
        setIcon(URL.createObjectURL(e.target.files[0]));

    }
    return (
        <div>

            <Dialog open={props.open} onClose={handleClose}>
                <AlertMes open={alert.open} severity={alert.severity} message={alert.message} setOpen={setAlert} />
                <DialogTitle>Add Category</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        To Edit category to this website, please enter a new category name, title and upload  a new icon
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        placeholder={props.categoryData?.name}
                        id="name"
                        label="Category Name"
                        type="text"
                        fullWidth
                        onChange={(e) => setCategoryData({ ...categoryData, name: e.target.value })}
                    />
                    <TextField
                        margin="dense"
                        id="title"
                        placeholder={props.categoryData?.title}
                        label="Category Title"
                        type="text"
                        fullWidth
                        onChange={(e) => setCategoryData({ ...categoryData, title: e.target.value })}
                    />
                    <Stack direction="row" alignItems="center" spacing={2}>
                        <Avatar
                            alt="Icon"
                            src={icon}
                            sx={{ width: 56, height: 56 }}
                        />
                        <div>Uplode Icon</div>
                        <IconButton color="primary" aria-label="upload picture" component="label">
                            <input type="file" accept="image/*" onChange={handleChange} className="hidden" />
                            <PhotoCamera />
                        </IconButton>
                    </Stack>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleClickAdd}>Add</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}