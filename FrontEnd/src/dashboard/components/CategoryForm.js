import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import Stack from '@mui/material/Stack';
import Avatar from '@mui/material/Avatar';
import AlertMes from '../../menu/components/Alert';

export default function CategoryForm(props) {
    const [categoryData, setCategoryData] = React.useState({ name: '', title: '', icon: null })
    const [alert, setAlert] = React.useState({ message: '', severity: '', open: false });
    const [icon, setIcon] = React.useState();
    const handleClickAdd = async () => {
        const categoryForm = new FormData()
        categoryForm.append('name', categoryData.name)
        categoryForm.append('title', categoryData.title)
        categoryForm.append('icon', categoryData.icon)
        if (categoryData.name.length === 0 || categoryData.title.length === 0 || categoryData.icon === null) {
            return setAlert({ ...alert, message: 'please fill all felds!', open: true, severity: 'error' })
        }
        props.setAddCategory({ ...props.addCategory, open: false, categoryInfo: categoryForm, yesOrNo: true });
        setCategoryData({ name: '', title: '', icon: null })
        setIcon(null)
    };

    const handleClose = () => {
        props.setAddCategory({ ...props.addCategory, open: false });
    };


    function handleChange(e) {
        setIcon(URL.createObjectURL(e.target.files[0]));
        setCategoryData({ ...categoryData, icon: e.target.files[0] })
    }
    return (
        <div>

            <Dialog open={props.open} onClose={handleClose}>
                <AlertMes open={alert.open} severity={alert.severity} message={alert.message} setOpen={setAlert} />
                <DialogTitle>Add Category</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        To add category to this website, please enter a category name, title and upload icon
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Category Name"
                        type="text"
                        fullWidth
                        onChange={(e) => setCategoryData({ ...categoryData, name: e.target.value })}
                    />
                    <TextField
                        margin="dense"
                        id="title"
                        label="Category Title"
                        type="text"
                        fullWidth
                        onChange={(e) => setCategoryData({ ...categoryData, title: e.target.value })}
                    />
                    <Stack direction="row" alignItems="center" spacing={2}>
                        <Avatar
                            alt="Icon"
                            src={icon}
                            sx={{ width: 56, height: 56 }}
                        />
                        <div>Uplode Icon</div>
                        <IconButton color="primary" aria-label="upload picture" component="label">
                            <input type="file" accept="image/*" onChange={handleChange} className="hidden" />
                            <PhotoCamera />
                        </IconButton>
                    </Stack>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleClickAdd}>Add</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}