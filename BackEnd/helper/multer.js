import multer from "multer";

const iconsPath = "./public/categoryIcons";
const ImagesPath = "./public/itemImages";

const storageIcons = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, iconsPath);
  },
  filename: (req, file, cb) => {
    const fileName = Date.now().toString() + ".png";
    cb(null, fileName);
  },
});


const storageImages = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, ImagesPath);
  },
  filename: (req, file, cb) => {
    const fileName = Date.now().toString() + ".png";
    cb(null, fileName);
  },
});


const uploadIcon = multer({
  storage: storageIcons,
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/heic" ||
      file.mimetype == "image/gif") {
      cb(null, true);
    }
    else {
      req.fileValidationError = "Allowed only .png";
      return cb(null, false, req.fileValidationError);
    }

  },
});


const uploadImage = multer({
  storage: storageImages,
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/heic" ||
      file.mimetype == "image/gif") {
      cb(null, true);
    }
    else {
      req.fileValidationError = "Allowed only .png, .jpg, .jpeg and .gif";
      return cb(null, false, req.fileValidationError);
    }
  }
});

export { uploadIcon, uploadImage };
