import express from "express";
import dotenv from "dotenv";
import connectDB from "./config/db.js";
import bodyParser from "body-parser";
import cors from "cors";
import path from "path";
import { fileURLToPath } from 'url';
import colors from 'colors';
import userRoutes from "./routes/userRoutes.js";
import categoryRoutes from "./routes/categoryRoutes.js"
import itemRoute from "./routes/itemRoutes.js"

const __filename = fileURLToPath(import.meta.url);

// 👇️ "/home/john/Desktop/javascript"
const __dirname = path.dirname(__filename);
dotenv.config();

connectDB();

const PORT = process.env.PORT || 5000;

const app = new express();
app.use("/public", express.static("./public"));
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.resolve(__dirname, './build')));
// parse application/json
app.use(bodyParser.json());
app.use("/api/users", userRoutes);
app.use("/api/categories", categoryRoutes);
app.use("/api/items", itemRoute);
app.get("/", (req, res) => {
    res.send("API is running ...");
});


app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, './build', 'index.html'));
});
app.listen(PORT, console.log(colors.blue.bold(`Server Running on Port ${colors.bgYellow.bold(PORT)}`)));
