import asyncHandler from "express-async-handler";
import Item from "../models/itemModel.js";
import Category from "../models/categoryModel.js";

// @desc      Delete single item
// @route     DELETE /api/item/:id
// @access    Admin/Private
const deleteItem = asyncHandler(async (req, res) => {
    const item = await Item.findById(req.params.id);
    if (item) {
        const category = await Category.findById(item.category_id);
        category.items = category.items.filter(function (item, index, arr) {
            return item != item._id
        });
        await category.save();
        await item.remove();
        res.json({ message: "Item removed successfully" });
    } else {
        res.status(404);
        res.json({ error: "Item not found" });
    }
});

// @desc      Create a item
// @route     POST /api/item/addItem
// @access    Admin/Private
const createItem = asyncHandler(async (req, res) => {
    if (req.fileValidationError) {
        return res.status(400).json({ error: req.fileValidationError });
    }
    const { name, description, price, category_id } = req.body;
    const foundCategory = await Category.findById(category_id);
    if (!foundCategory) {
        res.status(400);
        return res.json(({ error: "Category not found or removed!" }));
    }
    const image = req.file.path;
    const item = await Item.create({
        name,
        description,
        price,
        category_id,
        image,
    });
    foundCategory.items.push(item.id);
    foundCategory.save();
    if (item) {
        res.status(201).json(item);
    } else {
        res.status(400);
        res.json(({ error: "Invalid item data!" }));
    }
});


// @desc      GET Items
// @route     GET /api/items/
// @access    Public
const getItems = asyncHandler(async (req, res) => {
    const item = await Item.find({}).populate('category_id', 'name icon _id');

    res.json(item);
});

// @desc      Update a item
// @route     PUT /api/item/:id
// @access    Admin/Private
const updateItem = asyncHandler(async (req, res) => {
    const item = await Item.findById(req.params.id);
    if (req.fileValidationError) {
        return res.status(400).json({ error: req.fileValidationError });
    }
    if (req.hasOwnProperty('file')) {
        item.image = req.file.path;
    }
    if (item) {
        const foundCategory = await Category.findById(item.category_id);
        if (!foundCategory) {
            res.status(400);
            return res.json(({ error: "Category not found or removed!" }));
        }
        foundCategory.items.push(item.id);
        foundCategory.save();
        item.name = req.body.name || item.name;
        item.description = req.body.description || item.description;
        item.price = req.body.price || item.price;
        item.category_id = req.body.category_id || foundCategory._id;
        const updatedItem = await item.save();

        res.json({
            _id: updatedItem._id,
            name: updatedItem.name,
            description: updatedItem.description,
            price: updatedItem.price,
            image: updatedItem.image,
            category_id: updatedItem.category_id,
        });
    } else {
        res.status(404);
        res.json({ error: "Item not found" });
    }
});


export { deleteItem, createItem, getItems, updateItem }; 