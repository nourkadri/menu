import asyncHandler from "express-async-handler";
import Category from "../models/categoryModel.js";

// @desc      Delete single category
// @route     DELETE /api/categories/:id
// @access    Admin/Private
const deleteCategory = asyncHandler(async (req, res) => {
    const category = await Category.findById(req.params.id);
    if (category) {
        await category.remove();
        res.json({ message: "Category removed successfully" });
    } else {
        res.status(404);
        res.json({ error: "Category not found" });
    }
});

// @desc      Create a category
// @route     POST /api/categories
// @access    Admin/Private
const createCategory = asyncHandler(async (req, res) => {
    if (req.fileValidationError) {
        return res.status(400).json({ error: req.fileValidationError });
    }
    const { name, title } = req.body;
    const icon = req.file.path;
    const category = await Category.create({
        name,
        title,
        icon,
    });
    if (category) {
        res.status(201).json(category);
    } else {
        res.status(400);
        res.json({ error: "Invalid category data" });
    }
});

// @desc      Update a Category
// @route     PUT /api/category/:id
// @access    Admin/Private
const updateCategory = asyncHandler(async (req, res) => {
    const category = await Category.findById(req.params.id);
    if (req.fileValidationError) {
        return res.status(400).json({ error: req.fileValidationError });
    }
    if (req.hasOwnProperty('file')) {
        category.icon = req.file.path;
    }
    if (category) {
        category.name = req.body.name || category.name;
        category.title = req.body.title || category.title;
        const updatedCategory = await category.save();

        res.json({
            _id: updatedCategory._id,
            name: updatedCategory.name,
            title: updatedCategory.title,
            icon: updatedCategory.icon,
        });
    } else {
        res.status(404);
        res.json({ error: "Category not found" });
    }
});


// @desc      GET all Categories with items
// @route     GET /api/categories/
// @access    Public
const getAllCategoriesWithItems = asyncHandler(async (req, res) => {
    const categories = await Category.find({}, {  'createdAt': 0, 'updatedAt': 0, __v: 0 }).populate('items', 'name description price image -_id');
    res.json({ categories: categories });
});


// @desc      GET Category with items by Id category
// @route     GET /api/categories/:id
// @access    Public
const getCategoryById = asyncHandler(async (req, res) => {
    const categories = await Category.findById(req.params.id).populate('items', 'name description price image -_id');
    res.json(categories);
});

export { getAllCategoriesWithItems, createCategory, deleteCategory, updateCategory, getCategoryById }; 