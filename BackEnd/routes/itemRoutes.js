import express from "express";
import { uploadImage } from "../helper/multer.js";

const router = express.Router();
import {
    deleteItem,
    getItems,
    createItem,
    updateItem
} from "../controllers/itemControllers.js";
import { protect, admin } from "../middleware/authMiddleware.js";

router.route("/").get(getItems)
router.post("/addItem", protect, admin, uploadImage.single("image"), createItem);
router
    .route("/:id")
    .delete(protect, admin, deleteItem)
    .put(protect,admin,uploadImage.single("image"),updateItem)

export default router;