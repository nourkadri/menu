import express from "express";
import { uploadIcon } from "../helper/multer.js";

const router = express.Router();
import {
    getAllCategoriesWithItems,
    createCategory,
    deleteCategory,
    updateCategory,
    getCategoryById
} from "../controllers/categoryControllers.js";
import { protect, admin } from "../middleware/authMiddleware.js";

router.route("/").get(getAllCategoriesWithItems)
router.post("/addCategory", protect, admin, uploadIcon.single("icon"), createCategory);
router
    .route("/:id")
    .delete(protect, admin, deleteCategory)
    .put(protect, admin, uploadIcon.single("icon"), updateCategory)
    .get(getCategoryById)

export default router;